#!/usr/bin/python
import json
import sys
import urllib2
import sqlite3
import os
import time


def create_db():
    """
    Create database.
    """
    # Create database and tables. NOTE: timestamp in epoch.
    conn = sqlite3.connect('jenkins.sqlite3')
    conn.execute("CREATE TABLE job (name TEXT, status TEXT, time INT);")
    conn.close()


def save_jobs(jobs):
    """
    Save a list of jobs, on database.
    """
    conn = sqlite3.connect('jenkins.sqlite3')
    for j in jobs:
        conn.execute(
            "INSERT INTO job (name, status, time) VALUES ('%s', '%s', %d);"
            % (j.name, j.status, j.timestamp))
    conn.commit()
    conn.close()


class Job(object):
    """
    Job object.
    """
    def __init__(self, job_name, job_status, job_timestamp):
        self.name = job_name
        self.status = job_status
        self.timestamp = job_timestamp

    def get_display(self):
        return '[%s] - %s' % (self.name, self.status)


def start():
    # Check if database exists.
    if not os.path.isfile('./jenkins.sqlite3'):
        print 'Data base not found. Creating database...'
        create_db()
        print 'Success!'

    # Request timestamp (epoch)
    timestamp = time.time()
    # Connect to service.
    try:
        stream = urllib2.urlopen(instance + '/api/json')
    except urllib2.HTTPError, e:
        print 'URL Error: ' + str(e.code)
        sys.exit(2)

    # Parse connection result.
    try:
        response_json = json.load(stream)
    except:  # POKEMON
        print 'Error parsing json.'
        sys.exit(3)

    jobs_list = []
    # Display request timestamp
    print 'Time: %s' % time.strftime('%Y-%m-%d %H:%M:%S',
                                     time.localtime(timestamp))
    # For each job in service response...
    for job_json in response_json['jobs']:
        try:
            name = job_json['name']
            status = job_json['color']
        except KeyError:
            print 'Error parsing job information.'
            sys.exit(4)
        else:
            job = Job(name, status, timestamp)
            jobs_list.append(job)
            print job.get_display()  # Display on console.

    # Call function to save all jobs.
    save_jobs(jobs_list)

# #### INIT #####
if len(sys.argv) > 1:
    instance = sys.argv[1]
else:
    print 'Instance is missing. Run example: python jenkings.py http://localhost:8080'
    sys.exit(1)

start()
sys.exit(0)