from django.db import models


class MailAccount(models.Model):
    name = models.CharField('Name', max_length=150)
    email = models.EmailField('Email', unique=True)  # RFC3696/5321 compliant

    class Meta:
        verbose_name = 'Mail account'
        verbose_name_plural = 'Mail accounts'
        ordering = ['name', 'email']  # first order by name, and then email
