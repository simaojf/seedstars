from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from mail.forms import MailAccountForm
from mail.models import MailAccount


def index(request):
    """
    Index page.
    """
    return render(request, 'index.html', {})


def list_addresses(request):
    """
    List all mail addresses.
    """
    addresses_queryset = MailAccount.objects.values('name', 'email')
    context = {'addresses': addresses_queryset}
    return render(request, 'list.html', context)


def add_addresses(request):
    """
    Add a new address.
    """
    if request.method == 'POST':
        form = MailAccountForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Addresses added with success.')
            return redirect(reverse('index'))  # redirect to index
    else:
        form = MailAccountForm()

    context = {'form': form}
    return render(request, 'add.html', context)