from django import forms
from mail.models import MailAccount


class MailAccountForm(forms.ModelForm):
    class Meta:
        model = MailAccount
        fields = ['name', 'email']
        # Add required=required to html inputs. This avoids calls with
        # blank inputs, that would result in a validation error.
        widgets = {
            'name': forms.TextInput(attrs={'required': 'required'}),
            'email': forms.EmailInput(attrs={'required': 'required'})
        }