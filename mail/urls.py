from django.conf.urls import url
from mail import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^list/$', views.list_addresses, name='list_addresses'),
    url(r'^add/$', views.add_addresses, name='add_addresses'),
]