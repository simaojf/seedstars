# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MailAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name=b'Name')),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name=b'Email')),
            ],
            options={
                'ordering': ['name', 'email'],
                'verbose_name': 'Mail account',
                'verbose_name_plural': 'Mail accounts',
            },
        ),
    ]
